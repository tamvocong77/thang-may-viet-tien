Thang máy gia đình là gì?
[Thang máy gia đình](http://sieuthithangmay.com/thang-may-gia-dinh-viet-tien/) là một sản phẩm thang máy riêng biệt được sử dụng cho hộ gia đình, hoặc các nhà riêng sở hữu tư nhân. Thang máy gia đình là loại thang máy nhỏ gọn có không gian dành cho 2 đến 6 người. Không giống như các loại thang máy tiêu chuẩn có đối trọng, sử dụng máy kéo có hộp số có phòng máy, thang máy gia đình không cần thêm không gian phòng máy bên trên đỉnh hố thang, làm cho nó phù hợp hơn với mục đích sử dụng trong gia đình và cá nhân. Thông thường, chi phí bảo trì cho dòng thang máy này cũng thấp hơn so với thang máy thông thường (theo wikipedia.org)

Điểm khác biệt giữa thang máy gia đình với thang máy tải khách như thế nào?
[Thang máy tải khách](http://sieuthithangmay.com/thang-tai-khach/) thông thường có tốc độ từ trung bình đến cao, trung bình là 1m/s với số điểm dừng không bị giới hạn, phù hợp với mọi cao ốc cao tầng và nhiều mục đích sử dung, kể cả tải hàng hoá kèm theo. Thang máy tải khách cần phải xây dựng hố thang máy beton hoặc khung hố bằng thép chịu được tải trọng tương đối lớn vì phải bao gồm khung đối trọng của thang. Ngoài ra, hố thang máy tải khách tiêu chuẩn cần phải đào hố PIT sâu tối thiểu 700 mm so với mặt sàn tầng trệt hiện hữu ( thông thường sâu 1400 mm).

Thang máy gia đình với thiết kế tuỳ chỉnh, có khả năng di động và khả dụng cho tất cả mọi người bên trong toà nhà nhỏ thấp tầng ( 6 tầng trở xuống tương đương 15 m hành trình). Thang máy gia đình không cần phải đào hố PIT mà chúng được lắp đặt trực tiếp trên mặt sàn nhà với PIT chỉ sâu 150 mm để sàn cabin bằng sàn tầng đất bên ngoài. Chúng có tốc độ thấp (tối đa 0,3 m/s), khá phù hợp với các hộ gia đình có người già đi xe lăn hoặc người bị một loại khuyết tật nào đó. Thời gian lắp đặt thang máy gia đình nhanh chóng và đơn giản hơn thang máy tải khách thông thường vì chúng nhỏ gọn, được nhà sản xuất liên kết sẵn thành khối trước khi vận chuyển đến nơi cần lắp đặt. Công tác lắp đặt cho loại thang máy này mất khoảng 2 đến 5 ngày tuỳ thuộc vào số lượng điểm dừng và chiều cao thực tế cần lắp đặt.

Lưu ý: Các tiêu chuẩn bên trên áp dụng với mẫu thang máy gia đình mini thế hệ mới nhất hiện nay, sử dụng công nghệ không cần cáp kéo như mô tả bên dưới với giá thành gấp 3 lần so với loại cần cáp kéo.

Thang máy gia đình có mấy loại?
Về cơ bản, thang máy gia đình có 5 loại như sau:

Thang máy gia đình công nghệ cáp kéo
Có thể hiểu loại thang này chính là thang máy tải khách phổ biến có công suất nhỏ, bao gồm có đối trọng như thang máy thông thường. Chúng được sử dụng động cơ 3 phase với 2 loại cơ bản: máy kéo có hộp số bánh răng trục vít có phòng máy và loại không có hộp số (thang máy không phòng máy). Đối với loại thang có tải trọng nhỏ khoảng 180-250 Kg ta có thể sử dụng máy kéo không hộp số với động cơ 1 phase 220 V thông thường luôn có sẵn ở mọi gia đình.

Thang máy gia đình thuỷ lực
Thang máy gia đình thuỷ lực được chế tạo để sử dụng thường xuyên và lâu dài. Loại thang máy này hoạt động yên tĩnh vì không sử dụng cáp kéo. Chúng hoạt động được nhờ một máy bơm thuỷ lực kết nối với một pit-tong sẽ nâng hạ cabin thang máy trơn tru. Hệ thống truyền động theo tỉ lệ 2:1 nhằm giảm phạm vi di chuyển của pit-tong. Chẵng hạn khi pit-tong di chuyển 1 m thì cabin sẽ di chuyển được 2 m hành trình. Giá thành loại thang máy gia đình thuỷ lực sẽ đắt hơn các loại thang máy gia đình khác, tối thiểu là 30.000 USD chưa thuế cho phiên bản trung bình.

Ngoài ra, thang máy thuỷ lực cũng được sử dụng cho thang máy tải hàng hạng nặng với số điểm dừng thấp ( hành trình khoảng 15 m). Do chi phí lắp đặt và bảo trì cao nên hiện nay thang máy tải hàng vẫn sử dụng phổ biến công nghệ cáp kéo có phòng máy.

Thang máy gia đình thuỷ lực

Thang máy gia đình sử dụng xích có đối trọng
Thang máy loại này có cơ cấu truyền động bằng xích (giống như xích xe đạp loại lớn), thay vì sử dụng dây cáp thép hoặc pit-tong thuỷ lực để di chuyển cabin thang máy. Loại thang này hoạt động khá yên tĩnh gần giống như sử dụng thang máy thuỷ lực. Một trong những ưu điểm của thang máy sử dụng xích chính là chi phí bảo trì thấp hơn sử dụng cáp, vì khả năng thay xích có thể đến 20 năm nhờ độ bền của chúng. Để tăng hiệu quả cơ học thì thang máy sử dụng xích vẫn kèm theo đối trọng ( có khối lượng bằng tổng trọng lượng tĩnh cabin thang máy cộng thêm 40 % tải trọng cho phép chuyên chở).

Thang máy gia đình sử dụng xích

Thang máy gia đình chân không
Thang máy sử dụng công nghệ chân không (không cáp kéo) là một sản phẩm khá mới mẽ trên thị trường thang máy hiện nay. Chúng là một ống hình trụ được kết hợp kín hoàn hảo từ vật liệu nhôm và tấm polycarbonate cong, bao gồm hố thang là một ống trụ trục kín thẳng đứng và một cabin kín di chuyển bên trong nó nhờ lực đẩy tăng dần, được tạo ra bởi sự chên lệch áp xuất khí quyển bên trên và dưới đáy cabin. Sự khác biệt giữa 2 tầng áp suất được tạo nên do máy bơm chân không thực hiện.

Ưu điểm của thang máy chân không là sử dụng máy bơm chân không 1 phase, chỉ tiêu thụ điện theo mỗi chiều nâng cabin lên là rất ít nhờ cơ chế chuyển động gần như là không ma sát do áp xuất khí nén ở phần trên cabin . Trong trường hợp thang di chuyển xuống sẽ được điều khiển bởi 1 van điều áp, cho phép hạ xuống và kiểm soát tốc độ thang máy dừng đúng tầng chính xác.

Thang máy gia đình chân không

Thang máy gia đình bánh vít, trục vít
Thang máy gia đình sử dụng trục vít áp dụng nguyên lý hệ thống truyền động bằng cách thông qua chuyển động từ động cơ thang máy, nhờ dây curoa  truyền động bánh vít đi theo trục vít thẳng đứng dọc theo bên trong hố thang. Thông qua hệ thống bánh răng giúp cabin chuyển động lên xuống theo chiều quay của động cơ, tạo nên một hệ thống chuyển động nhịp nhàng và an toàn. Thang máy loại này có tốc độ khá thấp ( tối đa 0,3 m/s) được sử dụng động cơ nhỏ 1 phase 220 V,  tạo nên cảm giác an toàn, phù hợp cho mọi đối tượng sử dụng.

Ưu điểm của thang máy gia đình sử dụng trục vít là thời gian lắp đặt nhanh chóng, không cần phòng máy và đào hố PIT. Tuy nhiên, giá thành lắp đặt cho loại thang máy gia đình này là khá cao, chi phí bảo trì lớn, thời gian thay thế vật tư dài phụ thuộc rất nhiều vào nhà cung cấp độc quyền sản phẩm đã bán ra.

Thang máy gia đình giá bao nhiêu?
Giá thành thang máy gia đình phụ thuộc vào nhiều yếu tố như sau:

Chiều cao hành trình (m) hay số điểm dừng STOP (cửa tầng).
Khả năng can thiệp vào kết cấu toà nhà (đào hố PIT và có hoặc không phòng máy).
Tải trọng theo nhu cầu thực tế ( quy đổi ra công suất máy kéo KW), chọn lựa động cơ phù hợp 1 phase hay 3 phase.
Thang máy ngoại nhập nguyên chiếc hay thang máy liên doanh có sẵn ở thị trường Việt Nam.
Giá thành thang máy ngoại nhập nguyên chiếc hiện nay dao động vào tầm khoảng 25.000-55.000 USD. Mỗi công nghệ thang máy có những ưu điểm nổi trội khác nhau, nhưng chủ đầu tư ngoài vấn đề quan tâm về giá thành khi lắp mới, thì cần phải lưu ý đến các tiêu chí về độ bền, chi phí bảo trì, thời gian và giá thành thay thế vật tư sau khi hết thời hạn bảo hành từ nhà cung cấp thiết bị.Từ đó, chủ đầu tư sẽ lựa chon nhà cung cấp thang máy với công nghệ phù hợp và giá thành đầu tư thang máy hợp lý nhất cho mình.

Thang máy gia đình Việt Tiến sử dụng công nghệ gì?
Căn cứ các ưu điểm và khuyết điểm của các loại thang máy gia đinh phổ biến hiện nay như trên, cũng như dựa vào khả năng đầu tư tài chính của đa số người dùng trong nước, công ty thang máy Việt Tiến đã lựa chọn giải pháp lắp đặt thang máy cho gia đình của quý khách hàng với giải pháp như sau:

Thang máy sử dụng công nghệ cáp kéo có tốc độ tối thiểu 0,5 m/s đến 1 m/s vì nguồn vật tư phong phú và dễ thay thế, nâng cấp về sau.

Thang máy thuộc loại không phòng máy để tiết kiệm diện tích không gian, tránh ảnh hưởng đến kết cấu, kiến trúc mái toà nhà. Ngoài ra, loại thang này sử dụng máy kéo không hộp số tiết kiệm điện, có thể sử dụng nguồn điện 1 phase 220 V sẵn có thông thường ở gia đình.

Nếu thang máy có tốc độ 0,5 m/s (cao hơn thang máy ngoại nhập nguyên chiếc là 0,3 m/s) thì chỉ cần đào hố PIT sâu 600 mm để tăng độ bảo hiểm an toàn cho người dùng, cũng như bảo vệ khung cabin khi có sự cố vượt tốc độ cho phép. Trong trường hợp thang chạy với tốc độ 1 m/s thì cần phải đào hố PIT tối thiểu 900 mm so với mặt sàn. Trong trường hợp hố PIT không đạt được chiều sâu như quy định, thì chúng ta có thể xây 1 hoặc 2 bậc tam cấp để bước vào cửa tầng trệt của thang máy.

Kết cấu hố thang máy có thể được lắp đặt bằng khung thép định hình với vách kính cường lực để giảm tối thiểu không gian hao phí và thời gian lắp đặt so với xây dựng hố beton . Vách cabin thang máy có thể sử dung vách kính cường lực toàn phần hoặc bán phần kèm inox. Giải pháp này sẽ giúp thang máy gia đình như là một thang máy quan sát lồng kính, là điểm nhấn độc đáo của toà nhà với ánh sáng được phân bố đều từ trên xuống dưới. Giúp cho người dùng cảm giác yên tâm và an toàn khi sử dụng thang máy ở nhà ít người.

Tìm hiểu thêm:

Website: [http://sieuthithangmay.com/](http://sieuthithangmay.com/)